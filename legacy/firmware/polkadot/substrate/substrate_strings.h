#ifndef __POLKADOT_SUBSTRATE_STRINGS_H__
#define __POLKADOT_SUBSTRATE_STRINGS_H__

// Modules names
static const char* STR_MO_BALANCES = "Balances";

// Methods names
static const char* STR_ME_TRANSFER = "Transfer";
static const char* STR_ME_FORCE_TRANSFER = "Force transfer";
static const char* STR_ME_TRANSFER_KEEP_ALIVE = "Transfer keep alive";
static const char* STR_ME_TRANSFER_ALL = "Transfer all";

// Items names
static const char* STR_IT_amount = "Amount";
static const char* STR_IT_source = "Source";
static const char* STR_IT_dest = "Send to";
static const char* STR_IT_keep_alive = "Keep alive";

#endif