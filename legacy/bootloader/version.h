#define VERSION_MAJOR 2
#define VERSION_MINOR 0
#define VERSION_PATCH 1

// Keep same as firmware/version.h define
#define ONEKEY_VERSION "3.0.0"
